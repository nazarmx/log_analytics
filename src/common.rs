use std::fmt;

/// Represents a single typed log message
#[derive(Debug)]
pub struct LogMessage {
    pub log_type: String,
    pub message: String,
}

/// Possible errors when processing a JSON log message
#[derive(Debug)]
pub enum LogParseError {
    JsonParseError(serde_json::Error),
    NotAnObject,
    MissingLogType,
    NonStringLogType,
}

impl fmt::Display for LogParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use LogParseError::*;

        match self {
            JsonParseError(e) => write!(f, "failed to parse JSON: {}", e),
            NotAnObject => write!(f, "log message is not a JSON object"),
            MissingLogType => write!(
                f,
                "log message (JSON object) has no log type key or it's an empty string"
            ),
            NonStringLogType => write!(f, "log type key is not a string value"),
        }
    }
}

impl std::error::Error for LogParseError {}

impl From<serde_json::Error> for LogParseError {
    fn from(e: serde_json::Error) -> LogParseError {
        LogParseError::JsonParseError(e)
    }
}

impl LogMessage {
    pub fn parse(log_type_key: &str, json: &str) -> Result<LogMessage, LogParseError> {
        let mut value: serde_json::Value = serde_json::from_str(json)?;

        let object = value.as_object_mut().ok_or(LogParseError::NotAnObject)?;

        let log_type_value = object
            .remove(log_type_key)
            .ok_or(LogParseError::MissingLogType)?;

        let log_type = log_type_value
            .as_str()
            .ok_or(LogParseError::NonStringLogType)?;

        if log_type.is_empty() {
            return Err(LogParseError::MissingLogType);
        }

        let message = LogMessage {
            log_type: log_type.to_owned(),
            message: value.to_string(),
        };

        Ok(message)
    }
}

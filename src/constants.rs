/// API endpoint for submitting logs
pub const RESOURCE: &'static str = "/api/logs";
/// JSON content type of logs
pub const CONTENT_TYPE: &'static str = "application/json";
/// Custom header used for signing logs
pub const X_MS_DATE: &'static str = "x-ms-date";

#[cfg(feature = "mimalloc")]
mod allocation {
    #[global_allocator]
    static GLOBAL: mimalloc::MiMalloc = mimalloc::MiMalloc;
}

#[macro_use]
extern crate log;

mod common;
mod constants;
mod processing;
mod submitting;

#[tokio::main]
async fn main() {
    env_logger::init();
    let customer_id = std::env::var("CUSTOMER_ID").expect("missing CUSTOMER_ID");
    let shared_key = std::env::var("SHARED_KEY").expect("missing SHARED_KEY");
    let log_type_key = std::env::var("LOG_TYPE_KEY").expect("missing LOG_TYPE_KEY");

    let retry_delay_ms = std::env::var("RETRY_DELAY_MS").map(|val| {
        use std::str::FromStr;
        u64::from_str(&val)
            .expect("invalid retry delay milliseconds value, should be a non-negative integer")
    });

    let request_timeout_ms = std::env::var("REQUEST_TIMEOUT_MS").map(|val| {
        use std::str::FromStr;
        u64::from_str(&val)
            .expect("invalid request timeout milliseconds value, should be a non-negative integer")
    });

    let network_retries_count = std::env::var("NETWORK_RETRIES_COUNT").map(|val| {
        use std::str::FromStr;
        u64::from_str(&val)
            .expect("invalid network retries count value, should be a non-negative integer")
    });

    let (tx, rx) = tokio::sync::mpsc::channel::<common::LogMessage>(1);

    let mut consumer = submitting::LogConsumer::new(
        rx,
        customer_id,
        shared_key.as_bytes(),
        retry_delay_ms.ok(),
        request_timeout_ms.ok(),
        network_retries_count.unwrap_or_default(),
        std::env::var("TIME_GENERATED_FIELD").ok(),
    )
    .expect("failed to create LogConsumer");

    let writing = tokio::task::spawn(async move { consumer.run().await });

    let reading = tokio::task::spawn_blocking(move || {
        use std::io::{self, BufRead};
        let input = io::stdin();
        let lock = input.lock();

        let mut aggregator = processing::LogAggregator::new(tx);

        for line in lock.lines() {
            match line {
                Ok(line) => match common::LogMessage::parse(&log_type_key, &line) {
                    Ok(message) => match aggregator.push(message) {
                        Ok(new) => trace!("message {}", if new { "added" } else { "appended" }),
                        Err(e) => error!("failed to push log message: {}", e),
                    },
                    Err(e) => error!("failed to parse log message: {}", e),
                },
                Err(e) => error!("failed to read line from stdin: {}", e),
            };
            match aggregator.submit() {
                Ok(count) => trace!("submit() processed {} messages", count),
                Err(e) => {
                    error!("submit failed: {}", e);
                    break;
                }
            };
        }
        info!("received EOF / aggregation error, stopping reading, will submit any remaining messages");

        while aggregator.remaining_messages() > 0 {
            match aggregator.submit() {
                Ok(count) => trace!("submit() after EOF processed {} messages", count),
                Err(e) => {
                    error!("submit after EOF processed failed: {}", e);
                    break;
                }
            };
        }
    });

    match reading.await {
        Ok(()) => debug!("producer task completed"),
        Err(e) => error!("producer task crashed: {}", e),
    };
    match writing.await {
        Ok(result) => {
            debug!("consumer task finished");
            if let Err(e) = result {
                error!("consumer task error: {}", e);
            }
        }
        Err(e) => error!("consumer task crashed: {}", e),
    };
}

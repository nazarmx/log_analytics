use crate::common::LogMessage;
use std::collections::HashMap;
use std::fmt;
use tokio::sync::mpsc::{error::TrySendError, Sender};

/// Maximum of 30 MB per post to Azure Monitor Data Collector API. This is a size limit for a single post.
/// If the data from a single post that exceeds 30 MB, you should split the data up to smaller sized chunks and send them concurrently.
const AZURE_REQUEST_LIMIT: usize = 30 * 1000 * 1000;
/// Limit of a single JSON object log message
///
/// ```
/// '[' + <data> + ']'
/// ```
const LOG_MESSAGE_LENGTH_LIMIT: usize = AZURE_REQUEST_LIMIT - 2;

#[derive(Debug)]
pub enum AggregationError {
    EmptyMessage {
        log_type: String,
    },
    TooLarge {
        log_type: String,
        message: String,
        exceeded_by_bytes: usize,
    },
    ChannelClosed(LogMessage),
}

impl fmt::Display for AggregationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use AggregationError::*;

        match self {
            EmptyMessage { log_type } => {
                write!(f, "received an empty message of \"{}\" log type", log_type)
            }
            TooLarge {
                log_type,
                message,
                exceeded_by_bytes,
            } => write!(
                f,
                "message \"{}\" of type \"{}\" is too large (exceeded the limit by {} bytes)",
                message, log_type, exceeded_by_bytes
            ),
            ChannelClosed(message) => write!(
                f,
                "channel was closed when processing \"{}\" message: \"{}\"",
                &message.log_type, &message.message
            ),
        }
    }
}

impl std::error::Error for AggregationError {}

/// Tries to submit messages, otherwise aggregates them
pub struct LogAggregator {
    channel: Sender<LogMessage>,
    buffer: HashMap<String, String>,
}

impl LogAggregator {
    pub fn new(channel: Sender<LogMessage>) -> LogAggregator {
        LogAggregator {
            channel,
            buffer: HashMap::new(),
        }
    }

    pub fn remaining_messages(&self) -> usize {
        self.buffer.len()
    }

    pub fn submit(&mut self) -> Result<usize, AggregationError> {
        let mut count: usize = 0;

        // try to send the largest messages first
        for (log_type, mut message) in self.buffer.drain().max_by_key(|(_key, value)| value.len()) {
            message.push_str("]");

            let mut log_message = LogMessage { log_type, message };

            while let Err(e) = self.channel.try_send(log_message) {
                match e {
                    TrySendError::Full(returned_message) => {
                        trace!(
                            "channel was full when sending \"{}\" log message (length = {}): \"{}\"",
                            &returned_message.log_type,
                            returned_message.message.len(),
                            &returned_message.message,
                        );

                        // try to store the message back if it's not oversized
                        if returned_message.message.len() < LOG_MESSAGE_LENGTH_LIMIT {
                            let LogMessage {
                                log_type,
                                mut message,
                            } = returned_message;

                            trace!(
                                "placing back into buffer \"{}\" log message (length = {}): \"{}\"",
                                &log_type,
                                message.len(),
                                &message,
                            );

                            message.pop(); // remove ']'
                            self.buffer.insert(log_type, message);

                            return Ok(count);
                        } else {
                            // continue trying to submit it
                            warn!(
                                "trying to resend large \"{}\" log message (length = {}): \"{}\"",
                                &returned_message.log_type,
                                returned_message.message.len(),
                                &returned_message.message,
                            );
                            log_message = returned_message;
                        }
                    }
                    TrySendError::Closed(returned_message) => {
                        return Err(AggregationError::ChannelClosed(returned_message))
                    }
                }
            }
            count += 1;
        }

        Ok(count)
    }

    pub fn push(&mut self, log_message: LogMessage) -> Result<bool, AggregationError> {
        let LogMessage {
            log_type,
            mut message,
        } = log_message;

        if message.is_empty() {
            return Err(AggregationError::EmptyMessage { log_type });
        }

        let len = message.len();
        if len > LOG_MESSAGE_LENGTH_LIMIT {
            return Err(AggregationError::TooLarge {
                log_type,
                message,
                exceeded_by_bytes: len - LOG_MESSAGE_LENGTH_LIMIT,
            });
        }

        let mut is_new = false;
        let mut error: Option<AggregationError> = None;

        self.buffer
            .entry(log_type)
            .and_modify(|old_messages_json_array| {
                let old_len = old_messages_json_array.len();
                let total_len = old_len.saturating_add(len);

                if total_len > LOG_MESSAGE_LENGTH_LIMIT {
                    error = Some(AggregationError::TooLarge {
                        log_type: String::new(),
                        message: std::mem::replace(&mut message, String::new()),
                        exceeded_by_bytes: total_len - LOG_MESSAGE_LENGTH_LIMIT,
                    });
                } else {
                    trace!(
                        "buffering message \"{}\" (length = {}), current data size: {}",
                        &message,
                        len,
                        old_messages_json_array.len()
                    );

                    old_messages_json_array.push_str(",");
                    old_messages_json_array.push_str(&message);
                }
            })
            .or_insert_with(|| {
                is_new = true;
                message.insert_str(0, "[");
                message
            });

        if let Some(e) = error {
            Err(e)
        } else {
            Ok(is_new)
        }
    }
}

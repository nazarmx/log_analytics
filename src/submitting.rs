use crate::common::LogMessage;
use crate::constants;
use std::fmt;
use tokio::sync::mpsc::Receiver;
type HmacSha256 = hmac::Hmac<sha2::Sha256>;
use hmac::crypto_mac::InvalidKeyLength;
use hmac::Mac;
use tokio::time::{delay_for, Duration};

pub struct LogConsumer {
    channel: Receiver<LogMessage>,
    customer_id: String,
    hmac: HmacSha256,
    uri: String,
    client: reqwest::Client,
    retry_delay: Duration,
    network_retries_count: u64,
}

#[derive(Debug)]
pub enum LogConsumerError {
    HttpError(reqwest::Error),
    HeaderError(reqwest::header::InvalidHeaderValue),
    Base64DecodeError(base64::DecodeError),
    InvalidKeyLength(InvalidKeyLength),
    TlsError(native_tls::Error),
}

impl From<reqwest::Error> for LogConsumerError {
    fn from(e: reqwest::Error) -> LogConsumerError {
        LogConsumerError::HttpError(e)
    }
}

impl From<native_tls::Error> for LogConsumerError {
    fn from(e: native_tls::Error) -> LogConsumerError {
        LogConsumerError::TlsError(e)
    }
}

impl From<reqwest::header::InvalidHeaderValue> for LogConsumerError {
    fn from(e: reqwest::header::InvalidHeaderValue) -> LogConsumerError {
        LogConsumerError::HeaderError(e)
    }
}

impl From<base64::DecodeError> for LogConsumerError {
    fn from(e: base64::DecodeError) -> LogConsumerError {
        LogConsumerError::Base64DecodeError(e)
    }
}

impl From<InvalidKeyLength> for LogConsumerError {
    fn from(e: InvalidKeyLength) -> LogConsumerError {
        LogConsumerError::InvalidKeyLength(e)
    }
}

impl std::error::Error for LogConsumerError {}

impl fmt::Display for LogConsumerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use LogConsumerError::*;

        match self {
            HttpError(e) => write!(f, "HTTP client error: {}", e),
            HeaderError(e) => write!(f, "HTTP header error: {}", e),
            Base64DecodeError(e) => write!(f, "base64::decode() error: {}", e),
            InvalidKeyLength(e) => write!(f, "HMAC-SHA256 key error: {}", e),
            TlsError(e) => write!(f, "TLS library error: {}", e),
        }
    }
}

impl LogConsumer {
    pub fn new(
        channel: Receiver<LogMessage>,
        customer_id: String,
        shared_key: &[u8],
        retry_delay_ms: Option<u64>,
        request_timeout_ms: Option<u64>,
        network_retries_count: u64,
        time_generated_field: Option<String>,
    ) -> Result<LogConsumer, LogConsumerError> {
        let decoded_key = base64::decode(shared_key)?;
        let hmac = HmacSha256::new_varkey(&decoded_key)?;

        let capacity = if time_generated_field.is_some() { 2 } else { 1 };

        use reqwest::header::*;
        let mut headers = HeaderMap::with_capacity(capacity);
        headers.append(
            CONTENT_TYPE,
            HeaderValue::from_static(constants::CONTENT_TYPE),
        );
        if let Some(time_generated_field) = time_generated_field {
            let value = HeaderValue::from_str(&time_generated_field)?;
            headers.append("time-generated-field", value);
        }

        let uri = format!(
            "https://{}.ods.opinsights.azure.com{}?api-version=2016-04-01",
            customer_id,
            constants::RESOURCE
        );
        info!("Azure Monitor HTTP Data Collector API URI: {}", uri);

        let tls = native_tls::TlsConnector::builder()
            .min_protocol_version(Some(native_tls::Protocol::Tlsv12))
            .build()?;

        let client: reqwest::Client = reqwest::Client::builder()
            .use_preconfigured_tls(tls)
            .default_headers(headers)
            .timeout(Duration::from_millis(request_timeout_ms.unwrap_or(5000)))
            .build()?;

        let consumer = LogConsumer {
            channel,
            customer_id,
            hmac,
            uri,
            client,
            network_retries_count,
            retry_delay: Duration::from_millis(retry_delay_ms.unwrap_or(500)),
        };

        Ok(consumer)
    }

    fn signature(&self, rfc1123date: &str, len: usize) -> String {
        let mut hmac = self.hmac.clone();
        let string_to_hash = format!(
            "POST\n{}\n{}\n{}:{}\n{}",
            len,
            constants::CONTENT_TYPE,
            constants::X_MS_DATE,
            rfc1123date,
            constants::RESOURCE
        );
        trace!("using HMAC-SHA256 on: \"{}\"", string_to_hash);
        hmac.input(string_to_hash.as_bytes());
        let result = hmac.result();
        let base64 = base64::encode(&result.code());
        trace!("HMAC-SHA256 base64 encoded result: \"{}\"", base64);
        format!("SharedKey {}:{}", self.customer_id, base64)
    }

    pub async fn run(&mut self) -> Result<(), LogConsumerError> {
        while let Some(LogMessage { log_type, message }) = self.channel.recv().await {
            use reqwest::header::*;
            match HeaderValue::from_str(&log_type) {
                Ok(log_type_header) => {
                    trace!("submitting \"{}\" logs: {}", log_type, message);

                    let rfc1123date = chrono::Utc::now()
                        .format("%a, %d %b %Y %H:%M:%S GMT")
                        .to_string();
                    trace!("current RFC1123 date: {}", rfc1123date);

                    let len = message.len();
                    trace!("message Content-Length: {}", len);

                    let signature = self.signature(&rfc1123date, len);
                    trace!("message Authorization: {}", signature);

                    let request = self
                        .client
                        .post(&self.uri)
                        .body(message)
                        .header(CONTENT_LENGTH, len)
                        .header(AUTHORIZATION, HeaderValue::from_str(&signature)?)
                        .header("Log-Type", log_type_header)
                        .header(constants::X_MS_DATE, HeaderValue::from_str(&rfc1123date)?)
                        .build()?;

                    let mut network_retries_left = self.network_retries_count;

                    loop {
                        let request_clone =
                            request.try_clone().expect("failed to clone HTTP request");

                        match self.client.execute(request_clone).await {
                            Ok(response) => {
                                let status = response.status();
                                trace!("HTTP status: {}", status);
                                if status.is_success() {
                                    break;
                                };

                                let text = response.text().await?;
                                error!("{} {}", status, text);

                                if status == reqwest::StatusCode::TOO_MANY_REQUESTS {
                                    delay_for(self.retry_delay).await;
                                } else if !status.is_server_error() {
                                    break;
                                }
                            }
                            Err(e) => {
                                error!(
                                    "failed to send HTTP request: {}, retries left: {}",
                                    e, network_retries_left
                                );
                                if network_retries_left > 0 {
                                    network_retries_left -= 1;
                                } else {
                                    return Err(e.into());
                                }
                            }
                        }
                    }
                }
                Err(e) => error!(
                    "HTTP Log-Type header is invalid, skipping \"{}\" message \"{}\": {}",
                    log_type, message, e
                ),
            }
        }

        Ok(())
    }
}
